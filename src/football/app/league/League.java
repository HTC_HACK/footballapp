package football.app.league;

import football.app.league.club.Club;
import football.app.league.playerManager.Manager;
import football.app.league.playerManager.Player;
import football.app.league.playerManager.President;

import java.util.ArrayList;
import java.util.List;

public class League {

    private Integer id;
    private String name;
    private List<Club> clubs = new ArrayList<>();
    private List<Manager> managers = new ArrayList<>();
    private List<Player> players = new ArrayList<>();
    private President president;

    public League() {
    }


    public League(Integer id, String name, List<Club> clubs, List<Manager> managers, List<Player> players, President president) {
        this.id = id;
        this.name = name;
        this.clubs = clubs;
        this.managers = managers;
        this.players = players;
        this.president = president;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Club> getClubs() {
        return clubs;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClubs(List<Club> clubs) {
        this.clubs = clubs;
    }

    public List<Manager> getManagers() {
        return managers;
    }

    public void setManagers(List<Manager> managers) {
        this.managers = managers;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public President getPresident() {
        return president;
    }

    public void setPresident(President president) {
        this.president = president;
    }

    @Override
    public String toString() {
        return "League{" +
                "name='" + name + '\'' +
                '}';
    }
}
