package football.app.league.repository.interfaceFunc;

@FunctionalInterface
public interface SearchObject<T> {

    T searchObject();

}
