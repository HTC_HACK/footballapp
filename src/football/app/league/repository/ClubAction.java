package football.app.league.repository;

import football.app.league.club.Club;
import football.app.league.playerManager.Player;

import java.util.List;
import java.util.Scanner;

public class ClubAction {

    Scanner scannerInt = new Scanner(System.in);
    Scanner scannerStr = new Scanner(System.in);
    Scanner scannerDouble = new Scanner(System.in);

    //todo::buyplayer=>done
    public Player buyPlayer(Club club, Player player) {
        player.setTransfer(false);
        player.setClub_id(club.getId());
        return player;
    }

    //todo::return player =>done
    public Player returnPlayer(Club club, List<Player> players) {
        for (Player player : players) {
            if (player.getClub_id() == club.getId() && player.isTransfer())
                System.out.println(player.toString());
        }
        System.out.print("Enter id player : ");
        int playerId = scannerInt.nextInt();
        for (Player player : players) {
            if (player.getClub_id() == club.getId() && player.isTransfer() && player.getClub_id() == playerId) {
                player.setTransfer(false);
                return player;
            }
        }
        return null;
    }

    //todo::transfer player => done
    public Player transferPlayer(Club club, List<Player> players) {

        boolean isFree = false;
        for (Player player : players) {
            if (player.getClub_id() == club.getId())
                System.out.println(player.toString());
        }
        System.out.print("Enter player id : ");
        int playerId = scannerInt.nextInt();
        System.out.print("Is free yes 1 or no 0 : ");
        int free = scannerInt.nextInt();
        if (free == 1) isFree = true;

        for (Player player : players) {
            if (player.getId() == playerId && !player.isTransfer()) {
                player.setTransfer(true);
                if (isFree) {
                    player.setMarket_value(0);
                    player.setFree(true);
                }
                System.out.print("Enter market value : ");
                int value = scannerInt.nextInt();
                if (value > 0)
                    player.setMarket_value(value);
                return player;
            }
        }

        return null;
    }
}
