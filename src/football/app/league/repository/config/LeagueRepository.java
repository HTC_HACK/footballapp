package football.app.league.repository.config;

import football.app.league.playerManager.President;

public interface LeagueRepository {

    void leagueTable();

    void transferList();

    void matchStart();

    void clubList();

    void playerList();

    void managerList();
}
