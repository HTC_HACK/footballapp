package football.app.league.repository.config;

public interface CrudRepository {

    void createPlayer();

    void createManager();

    void createClub();

}
