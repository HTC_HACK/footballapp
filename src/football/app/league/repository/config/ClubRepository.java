package football.app.league.repository.config;

import football.app.league.club.Club;
import football.app.league.playerManager.Manager;
import football.app.league.playerManager.Player;

import java.util.List;

public interface ClubRepository {

    void clubInfo(Club club);

    void balance(Club club);

    void buyPlayer(Club club, Player player,Club club1);

    void transferPlayer(Club club, List<Player> players);

    void buyManager(Club club, Manager manager);

    void injuryList(Club club,List<Player> players);

    void clubPlayerStats(Club club);

    void clubStats(Club club);

}
