package football.app.league.repository.builder;


import football.app.league.playerManager.Person;

import java.util.List;

@FunctionalInterface

public interface TripleFunction<T, M> {

    M tripleFunction(T list, List<M> lists);

}
