package football.app.league.repository.builder;

@FunctionalInterface
public interface ObjectBuilder<T> {

    T create();

}
