package football.app.league.repository;

import football.app.helpers.enums.Position;
import football.app.helpers.messages.MessageHelper;
import football.app.league.club.Club;
import football.app.league.playerManager.Manager;
import football.app.league.playerManager.Player;
import football.app.league.repository.builder.ObjectBuilder;

import java.util.List;
import java.util.Scanner;

public class LeagueAction {

    Scanner scannerInt = new Scanner(System.in);
    Scanner scannerStr = new Scanner(System.in);
    Scanner scannerDouble = new Scanner(System.in);

    //todo::player create => done
    public List<Player> createP(List<Player> objects, int playerId) {

        ObjectBuilder<Player> playerBuilder = Player::new;
        Player player = playerBuilder.create();

        System.out.print("Enter player name : ");
        String name = scannerStr.nextLine();

        if (!playerExist(objects, name)) {

            player.setName(name);

            Position position = null;
            System.out.println("1.GOALKEEPER.2.DEFENDER.3.MID-DEFENDER.4.FORWARD");

            System.out.print("Choose player position : ");
            int positionOption = scannerInt.nextInt();
            position = getPosition(position, positionOption);
            player.setPosition(position);

            System.out.print("Enter age : ");
            int age = scannerInt.nextInt();
            player.setAge(age);

            System.out.print("Enter market value : ");
            double marketValue = scannerDouble.nextDouble();
            player.setMarket_value(marketValue);

            player.setTransfer(true);
            player.setFree(false);
            player.setInjury(false);
            player.setClub_id(null);
            player.setId(playerId);

            objects.add(player);
        }

        return objects;
    }

    public boolean playerExist(List<Player> objects, String name) {
        for (Player object : objects) {
            if (object.getName().equalsIgnoreCase(name))
                return true;
        }

        return false;
    }

    private Position getPosition(Position position, int positionOption) {
        switch (positionOption) {
            case 1: {
                position = Position.GOALKEEPER;
                break;
            }
            case 2: {
                position = Position.DEFENDER;
                break;
            }
            case 3: {
                position = Position.MID_DEFENDER;
                break;
            }
            case 4: {
                position = Position.FORWARD;
                break;
            }
            default: {
                position = Position.GOALKEEPER;
                break;
            }
        }

        return position;
    }

    //todo::manager create => done
    public List<Manager> createM(List<Manager> objects, int managerId) {
        ObjectBuilder<Manager> managerBuilder = Manager::new;
        Manager manager = managerBuilder.create();

        System.out.print("Enter manager name : ");
        String name = scannerStr.nextLine();

        if (!managerExist(objects, name)) {

            manager.setName(name);

            manager.setPosition(Position.MANAGER);

            System.out.print("Enter age : ");
            int age = scannerInt.nextInt();
            manager.setAge(age);

            System.out.print("Enter market value : ");
            double marketValue = scannerDouble.nextDouble();
            manager.setMarket_value(marketValue);

            manager.setTransfer(true);
            manager.setFree(false);
            manager.setClub_id(null);
            manager.setId(managerId);
            objects.add(manager);
        }

        return objects;
    }

    public boolean managerExist(List<Manager> objects, String name) {
        for (Manager object : objects) {
            if (object.getName().equalsIgnoreCase(name))
                return true;
        }

        return false;
    }

    //todo::create club => done
    public List<Club> createC(List<Club> objects, int clubId) {

        System.out.print("Enter club title : ");
        String title = scannerStr.nextLine();

        if (!clubExist(objects, title)) {
            ObjectBuilder<Club> clubBuilder = Club::new;
            Club club = clubBuilder.create();
            club.setTitle(title);
            club.setId(clubId);
            objects.add(club);
        }

        return objects;
    }

    public boolean clubExist(List<Club> objects, String name) {
        for (Club object : objects) {
            if (object.getTitle().equalsIgnoreCase(name))
                return true;
        }

        return false;
    }
}
