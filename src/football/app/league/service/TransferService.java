package football.app.league.service;

import football.app.helpers.messages.MessageHelper;
import football.app.league.Match;
import football.app.league.club.Club;
import football.app.league.playerManager.Player;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class TransferService {

    int matchId = 1;

    Scanner scannerInt = new Scanner(System.in);
    Scanner scannerStr = new Scanner(System.in);
    Scanner scannerDouble = new Scanner(System.in);

    //todo::transferList => done
    public void transferList(List<Player> players) {
        System.out.println(MessageHelper.PLAYERS_TRANSFER);
        players.stream()
                .filter(player -> player.isTransfer())
                .forEach(System.out::println);
    }

    //todo::leagueTable => done
    public void leagueTable(List<Club> clubs) {

        Comparator<Club> compareByPoints = Comparator.comparing(Club::getPoints)
                .thenComparing(Club::getGoals);

        List<Club> sortedClub = clubs.stream()
                .sorted(compareByPoints)
                .collect(Collectors.toList());

        Collections.reverse(sortedClub);

        System.out.println("#  | Club | Pts | Match | Goals");
        sortedClub.forEach(club -> System.out.println(club.leagueT()));

    }

    //todo::matchStart=>proccess

    public List<Match> matchStart(List<Club> clubs, List<Match> matches, int clubsCount, int match_number) {

        Club firstclub = null;
        Club secondclub = null;

        int min = 0;
        int max = 5;

        int b = 0;
        int c = 0;

        Collections.sort(clubs, new Comparator<Club>() {
            @Override
            public int compare(Club a1, Club a2) {
                return a1.getGames() - a2.getGames();
            }
        });

        int a = 0;
        for (Club club : clubs) {
            a = club.getGames();
            break;
        }
        for (Club club : clubs) {
            if (a == club.getGames())
                System.out.println(club.table());
        }
        System.out.print("Choose first club : ");
        int firstId = scannerInt.nextInt();
        for (Club club : clubs) {
            if (a == club.getGames() && club.getId() == firstId) {
                firstclub = club;
                break;
            }
        }

        for (Club club : clubs) {
            if (a == club.getGames())
                System.out.println(club.table());
        }
        System.out.print("Choose second club : ");
        int secondId = scannerInt.nextInt();
        for (Club club : clubs) {
            if (a == club.getGames() && club.getId() == secondId && firstclub.getId() != secondId) {
                secondclub = club;
                break;
            }
        }

        if (firstclub != null && secondclub != null) {
            b = (int) (Math.random() * (max - min + 1) + min);
            c = (int) (Math.random() * (max - min + 1) + min);

            Match match = new Match(matchId, 1, firstclub.getId(), secondclub.getId(), b, c, match_number);
            increaseClub(clubs, firstclub, secondclub, b, c);
            matches.add(match);
        }

        return matches;
    }

    private void increaseClub(List<Club> clubs, Club firstclub, Club secondclub, int b, int c) {
        if (b > c) {
            for (Club club1 : clubs) {
                if (club1.getId() == firstclub.getId()) {
                    club1.addPoints(club1, 3);
                    club1.addGoal(club1, (b - c));
                    club1.addGames(club1, 1);
                    break;
                }
            }
            for (Club club1 : clubs) {
                if (club1.getId() == secondclub.getId()) {
                    club1.addPoints(club1, 0);
                    club1.addGoal(club1, (c - b));
                    club1.addGames(club1, 1);
                    break;
                }
            }

        } else if (c > b) {
            for (Club club1 : clubs) {
                if (club1.getId() == firstclub.getId()) {
                    club1.addPoints(club1, 0);
                    club1.addGoal(club1, (b - c));
                    club1.addGames(club1, 1);
                    break;
                }
            }
            for (Club club1 : clubs) {
                if (club1.getId() == secondclub.getId()) {
                    club1.addGoal(club1, (c - b));
                    club1.addPoints(club1, 3);
                    club1.addGames(club1, 1);
                    break;
                }
            }
        } else {
            for (Club club1 : clubs) {
                if (club1.getId() == firstclub.getId()) {
                    club1.addGoal(club1, 0);
                    club1.addPoints(club1, 1);
                    club1.addGames(club1, 1);
                    break;
                }
            }
            for (Club club1 : clubs) {
                if (club1.getId() == secondclub.getId()) {
                    club1.addGoal(club1, 0);
                    club1.addPoints(club1, 1);
                    club1.addGames(club1, 1);
                    break;
                }
            }
        }
    }

}