package football.app.league.service;

import football.app.helpers.messages.MessageHelper;
import football.app.league.League;
import football.app.league.Match;
import football.app.league.club.Club;
import football.app.league.club.ClubComparator;
import football.app.league.playerManager.Manager;
import football.app.league.playerManager.Player;
import football.app.league.playerManager.President;
import football.app.league.repository.LeagueAction;
import football.app.league.repository.builder.TripleFunction;
import football.app.league.repository.config.CrudRepository;
import football.app.league.repository.config.LeagueRepository;

import java.util.*;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class LeagueService implements CrudRepository, LeagueRepository {

    List<League> leagues = new ArrayList<>();
    int leagueId = 1;
    List<Club> clubs = new ArrayList<>();
    int clubId = 1;
    List<Manager> managers = new ArrayList<>();
    int managerId = 1;
    List<Player> players = new ArrayList<>();
    int playerId = 1;

    List<Match> matches = new ArrayList<>();
    int match_number = 1;

    Scanner scannerInt = new Scanner(System.in);

    TransferService transferService = new TransferService();

    //todo::Create League and President => done
    public LeagueService() {
        President president = new President(1, "Rafael", true);
        League league = new League(leagueId, "EPL", clubs, managers, players, president);
        leagues.add(league);
        leagueId++;
    }

    //todo::league info => done
    public void leagueInfo() {
        leagues.forEach(league -> System.out.println(league.toString()));
    }

    //todo::createPlayerdone
    @Override
    public void createPlayer() {
        LeagueAction createPlayer = new LeagueAction();
        players = createPlayer.createP(players, playerId);
        playerId++;
        System.out.println(MessageHelper.PLAYER);
    }

    //todo::managerCreatedone
    @Override
    public void createManager() {
        LeagueAction createManager = new LeagueAction();
        managers = createManager.createM(managers, managerId);
        managerId++;
        System.out.println(MessageHelper.MANAGER_CREATED);
    }

    //todo::createClubdone
    @Override
    public void createClub() {
        LeagueAction createClub = new LeagueAction();
        clubs = createClub.createC(clubs, clubId);
        clubId++;
        System.out.println(MessageHelper.CLUB_CREATED);
    }

    @Override
    public void leagueTable() {
        transferService.leagueTable(clubs);
    }

    //todo::transferList
    @Override
    public void transferList() {
        transferService.transferList(players);
    }

    public void transferClub(Club club) {
        int flag = 0;
        System.out.println(MessageHelper.PLAYERS_TRANSFER);
        for (Player player : players) {
            if (club.getId() != player.getClub_id() && player.isTransfer()) {
                System.out.println(player.toString());
                flag = 1;
            }
        }
        if (flag == 0) {
            System.out.println(MessageHelper.NO_PLAYER_TRANSFER);
        }
    }

    //todo::matchStart => process
    @Override
    public void matchStart() {
        ClubComparator clubComparator = new ClubComparator();
        Collections.sort(clubs, clubComparator);
        int games = 0;

        Supplier<Integer> supplier = () -> {
            return clubs.get(0).getGames();
        };

        if (clubs.size() >= 6 && (clubs.size() * 2 - 2) >= games) {
            matches = transferService.matchStart(clubs, matches, clubs.size(), match_number);
            match_number++;
            System.out.println(MessageHelper.MATCH_STARTED);
            System.out.println(MessageHelper.MATCH_ENDED);

        } else if (clubs.size() < 5) {
            System.out.println(MessageHelper.NOT_ENOUGH);
        } else {
            System.out.println(MessageHelper.CHAMPIONS_ENDED);
        }
    }

    @Override
    public void clubList() {
        if (clubs.isEmpty())
            System.out.println(MessageHelper.NO_CLUBS);
        else
            clubs.forEach(club -> System.out.println(club.clubT()));
    }

    @Override
    public void playerList() {
        if (players.isEmpty())
            System.out.println(MessageHelper.NO_PLAYERS);
        else
            players.forEach(player -> System.out.println(player.toString()));
    }

    @Override
    public void managerList() {
        if (managers.isEmpty()) {
            System.out.println(MessageHelper.NO_MANAGERS);
        } else {
            for (Manager manager : managers) {
                if (manager.isTransfer()) {
                    System.out.println(manager.toString());
                } else {
                    System.out.println("Not available : " + manager.toString());
                }
            }
        }
    }

    //todo::incoming in transfer => done
    public void addBalanceClub(Player player) {
        for (Club club : clubs) {
            if (club.getId() == player.getClub_id()) {
                club.addBalance(club, player.getMarket_value());
                break;
            }
        }
    }

    //todo::matches in League => done
    public void matchesResult() {
        Collections.sort(matches, new Comparator<Match>() {
            @Override
            public int compare(Match a1, Match a2) {
                return a1.getMatch_number() - a2.getMatch_number();
            }
        });
        Collections.reverse(matches);
        System.out.println("Results");
        for (Match match : matches) {
            String clubOne = findName(match.getClubOne_id());
            String clubTwo = findName(match.getClubTwo_id());
            System.out.println(clubOne + " " + match.getClubOne_goal() + " : " + match.getClubTwo_goal() + " " + clubTwo + ", match : " + match.getMatch_number() + " match");
        }
    }

    //todo::findclub => done
    public String findName(int id) {
        String result = "";
        for (Club club : clubs) {
            if (club.getId() == id) {
                result = club.getTitle();
                break;
            }
        }
        return result;
    }

    //todo::choose club=>done
    public Club chooseClub() {
        clubList();
        System.out.print("Enter club id : ");
        int selectclub = scannerInt.nextInt();

        for (Club club : clubs) {
            if (selectclub != 0 && club.getId() == selectclub) {
                return club;
            }
        }

        return null;
    }

    //todo::choose player =>done
    public Player choosePlayer(Club club) {
        transferClub(club);
        System.out.print("Enter player id : ");
        int selectplayer = scannerInt.nextInt();

        for (Player player : players) {
            if (player.getId() == selectplayer && !player.isInjury() && player.isTransfer()) {
                return player;
            }
        }

        return null;
    }

    public Club findClub(Player player) {
        for (Club club : clubs) {
            if (club.getId() == player.getClub_id())
                return club;
        }
        return null;
    }

    //todo::choose manager=>done
    public Manager chooseManager() {
        managerList();
        System.out.print("Enter manager id : ");
        int selectmanager = scannerInt.nextInt();

        //Declerative
        TripleFunction<Integer, Manager> tripleFunc = (mangerIdInput, managerlists) -> {
            Manager managerChoose = null;
            Predicate<Manager> managerPredicate = manager -> manager.getId() == mangerIdInput && manager.isTransfer();
            managerChoose = managerlists.stream()
                    .filter(managerPredicate)
                    .findFirst()
                    .orElse(null);
            return managerChoose;
        };

        return tripleFunc.tripleFunction(selectmanager, managers);
    }

    //todo::playerlist => done
    public List<Player> playersList() {
        return players;
    }

    //todo::playerlistClub => done
    public void playersListClub(Club club) {

        Predicate<Player> playerPredicate = player -> player.getClub_id() == club.getId();

        players.stream()
                .filter(playerPredicate)
                .forEach(System.out::println);
    }

    //todo::managerClub => done
    public void managerClub(Club club) {
        managers.stream()
                .filter(manager -> manager.getClub_id() == club.getId())
                .forEach(System.out::println);
    }

    //todo::restartLeague => done
    public void restartLeague() {
        matches.removeAll(matches);
        clubs.forEach(club -> {
            club.setAll(club);
        });
        System.out.println(MessageHelper.RESTARTED);
    }

}