package football.app.league.service;

import football.app.helpers.messages.MessageHelper;
import football.app.league.club.Club;
import football.app.league.playerManager.Manager;
import football.app.league.playerManager.Player;
import football.app.league.repository.ClubAction;
import football.app.league.repository.config.ClubRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ClubService implements ClubRepository {


    ClubAction clubAction = new ClubAction();

    //todo::clubInfo => done
    @Override
    public void clubInfo(Club club) {
        System.out.println(club.toString());
    }

    //todo::balance =>done
    @Override
    public void balance(Club club) {
        System.out.println("Balance : " + club.getBalance() + " mln $.");
    }

    //todo::buyPlayer=>done
    @Override
    public void buyPlayer(Club club, Player player, Club club1) {
        if (balanceCheck(club, player.getMarket_value())) {
            club.reduceBalance(club, player.getMarket_value());
            if (player.getClub_id() != null)
                club1.addBalance(club1, player.getMarket_value());
            clubAction.buyPlayer(club, player);
            System.out.println(MessageHelper.PLAYER_BUY);
        } else {
            System.out.println(MessageHelper.CHECK_BALANCE);
        }

    }

    //todo::transferPlayer =>done
    @Override
    public void transferPlayer(Club club, List<Player> players) {
        Player player = clubAction.transferPlayer(club, players);
        if (player != null) {
            System.out.println(player.toString());
            System.out.println(MessageHelper.PLAYERS_TRANSFER);
        } else {
            System.out.println(MessageHelper.FAILED);
        }
    }

    //todo::return transfer list=>done
    public void returnPlayer(Club club, List<Player> players) {
        Player player = clubAction.returnPlayer(club, players);
        if (player != null) {
            System.out.println(player.toString());
            System.out.println(MessageHelper.BACK_IN);
        } else {
            System.out.println(MessageHelper.FAILED);
        }
    }

    //todo::buyManager =>done
    @Override
    public void buyManager(Club club, Manager manager) {
        if (balanceCheck(club, manager.getMarket_value())) {
            club.reduceBalance(club, manager.getMarket_value());
            manager.setTransfer(false);
            manager.setClub_id(club.getId());
            club.setManager(manager);
        } else {
            System.out.println(MessageHelper.CHECK_BALANCE);
        }
    }

    //todo::injurylist =>done
    @Override
    public void injuryList(Club club, List<Player> players) {
        for (Player player : players) {
            if (player.getClub_id() == club.getId() && player.isInjury())
                System.out.println(player.toString());
        }
    }

    //todo::player stats =>done
    @Override
    public void clubPlayerStats(Club club) {
        System.out.println(MessageHelper.UNDER_CONSTRUCTION);
    }

    //todo::club stats =>done
    @Override
    public void clubStats(Club club) {
        System.out.println(club.toString());
    }

    //todo::balance checker =>done
    public boolean balanceCheck(Club club, double amount) {
        if (club.getBalance() >= amount) return true;
        return false;
    }

}
