package football.app.league;

public class Match {

    private Integer id;
    private Integer league_id;
    private Integer clubOne_id;
    private Integer clubTwo_id;
    private int clubOne_goal;
    private int clubTwo_goal;
    private int match_number;

    public Match() {
    }

    public Match(Integer id, Integer league_id, Integer clubOne_id, Integer clubTwo_id, int clubOne_goal, int clubTwo_goal, int match_number) {
        this.id = id;
        this.league_id = league_id;
        this.clubOne_id = clubOne_id;
        this.clubTwo_id = clubTwo_id;
        this.clubOne_goal = clubOne_goal;
        this.clubTwo_goal = clubTwo_goal;
        this.match_number = match_number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLeague_id() {
        return league_id;
    }

    public void setLeague_id(Integer league_id) {
        this.league_id = league_id;
    }

    public Integer getClubOne_id() {
        return clubOne_id;
    }

    public void setClubOne_id(Integer clubOne_id) {
        this.clubOne_id = clubOne_id;
    }

    public Integer getClubTwo_id() {
        return clubTwo_id;
    }

    public void setClubTwo_id(Integer clubTwo_id) {
        this.clubTwo_id = clubTwo_id;
    }

    public int getClubOne_goal() {
        return clubOne_goal;
    }

    public void setClubOne_goal(int clubOne_goal) {
        this.clubOne_goal = clubOne_goal;
    }

    public int getClubTwo_goal() {
        return clubTwo_goal;
    }

    public void setClubTwo_goal(int clubTwo_goal) {
        this.clubTwo_goal = clubTwo_goal;
    }

    public int getMatch_number() {
        return match_number;
    }

    public void setMatch_number(int match_number) {
        this.match_number = match_number;
    }

    @Override
    public String toString() {
        return "Match{" +
                "id=" + id +
                ", league_id=" + league_id +
                ", clubOne_id=" + clubOne_id +
                ", clubTwo_id=" + clubTwo_id +
                ", clubOne_goal=" + clubOne_goal +
                ", clubTwo_goal=" + clubTwo_goal +
                ", match_number=" + match_number +
                '}';
    }
}
