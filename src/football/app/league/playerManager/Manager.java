package football.app.league.playerManager;

import football.app.helpers.enums.Position;

public class Manager extends Person {

    public Manager() {
    }

    public Manager(Integer id, String name, Position position, int age, double market_value, boolean isTransfer, boolean isFree, Integer club_id) {
        super(id, name, position, age, market_value, isTransfer, isFree, club_id);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
