package football.app.league.playerManager;

import football.app.helpers.enums.Position;

public abstract class Person {

    protected Integer id;
    protected String name;
    protected Position position;
    protected int age;
    protected double market_value;
    protected boolean isTransfer = true;
    protected boolean isFree = false;
    protected Integer club_id;

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", position=" + position +
                ", age=" + age +
                ", market_value=" + market_value +
                ", isTransfer=" + isTransfer +
                ", isFree=" + isFree +
                ", club_id=" + club_id +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getMarket_value() {
        return market_value;
    }

    public void setMarket_value(double market_value) {
        this.market_value = market_value;
    }

    public boolean isTransfer() {
        return isTransfer;
    }

    public void setTransfer(boolean transfer) {
        isTransfer = transfer;
    }

    public boolean isFree() {
        return isFree;
    }

    public void setFree(boolean free) {
        isFree = free;
    }

    public Integer getClub_id() {
        return club_id;
    }

    public void setClub_id(Integer club_id) {
        this.club_id = club_id;
    }

    public Person() {
    }

    public Person(Integer id, String name, Position position, int age, double market_value, boolean isTransfer, boolean isFree, Integer club_id) {
        this.id = id;
        this.name = name;
        this.position = position;
        this.age = age;
        this.market_value = market_value;
        this.isTransfer = isTransfer;
        this.isFree = isFree;
        this.club_id = club_id;
    }
}
