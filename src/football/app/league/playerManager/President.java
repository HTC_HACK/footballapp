package football.app.league.playerManager;

public class President {

    private Integer id;
    private String name;
    private boolean isPresident;

    public President() {
    }

    public President(Integer id, String name, boolean isPresident) {
        this.id = id;
        this.name = name;
        this.isPresident = isPresident;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPresident() {
        return isPresident;
    }

    public void setPresident(boolean president) {
        isPresident = president;
    }

    @Override
    public String toString() {
        return "President{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", isPresident=" + isPresident +
                '}';
    }
}
