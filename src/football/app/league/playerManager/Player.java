package football.app.league.playerManager;

import football.app.helpers.enums.Position;
import football.app.league.club.Club;

public class Player extends Person {

    private boolean isInjury;
    private int goals;
    private int assists;

    @Override
    public String toString() {
        return "Player{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", position=" + position +
                ", age=" + age +
                ", market_value=" + market_value +
                ", isTransfer=" + isTransfer +
                ", isFree=" + isFree +
                ", club_id=" + club_id +
                ", isInjury=" + isInjury +
                ", goals=" + goals +
                ", assists=" + assists +
                '}';
    }

    public double getAssists() {
        return assists;
    }

    public void setAssists(int assists) {
        this.assists = assists;
    }

    public int getGoals() {
        return goals;
    }

    public void addGoal(Player player, int goal) {
        player.setGoals(player.getGoals() + goal);
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public Player() {
    }

    public Player(Integer id, String name, Position position, int age, double market_value, boolean isTransfer, boolean isFree, Integer club_id) {
        super(id, name, position, age, market_value, isTransfer, isFree, club_id);
    }

    public Player(boolean isInjury) {
        this.isInjury = isInjury;
    }

    public boolean isInjury() {
        return isInjury;
    }

    public void setInjury(boolean injury) {
        isInjury = injury;
    }


    public Player(Integer id, String name, Position position, int age, double market_value, boolean isTransfer, boolean isFree, Integer club_id, boolean isInjury) {
        super(id, name, position, age, market_value, isTransfer, isFree, club_id);
        this.isInjury = isInjury;
    }
}
