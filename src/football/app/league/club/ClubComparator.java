package football.app.league.club;

import java.util.Comparator;

public class ClubComparator implements Comparator<Club> {

    @Override
    public int compare(Club club, Club club2) {
        return club.getGames() - club2.getGames();
    }

}