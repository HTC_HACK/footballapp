package football.app.league.club;

import football.app.league.playerManager.Manager;
import football.app.league.playerManager.Player;

import java.util.ArrayList;
import java.util.List;

public class Club {

    private Integer id;
    private String title;
    private Manager manager;
    private List<Player> players = new ArrayList<>();
    private double balance = 200;
    private Integer league_id = 1;

    //Additional Info
    private int points;
    private int game_goal;
    private int games;
    private int goals;

    public void addGames(Club club, int game) {
        club.setGames(club.getGames() + game);
    }

    public void addGoal(Club club, int goal) {
        club.setGoals(club.getGoals() + goal);
    }

    public void addPoints(Club club, int point) {
        club.setPoints(club.getPoints() + point);
    }

    public void reduceBalance(Club club, double amount) {
        club.setBalance(club.getBalance() - amount);
    }

    public void addBalance(Club club, double amount) {
        club.setBalance(club.getBalance() + amount);
    }

    public void addBalanceId(int id, double amount) {
        if (getId() == id)
            setBalance(getBalance() + amount);
    }

    public Club() {
    }

    public Club(Integer id, String title) {
        this.id = id;
        this.title = title;
    }

    public Club(Integer id, String title, Manager manager, List<Player> players, double balance, Integer league_id, int points, int games, int goals, int game_goal) {
        this.id = id;
        this.title = title;
        this.manager = manager;
        this.players = players;
        this.balance = balance;
        this.league_id = league_id;
        this.points = points;
        this.games = games;
        this.goals = goals;
        this.game_goal = game_goal;
    }

    public int getGame_goal() {
        return game_goal;
    }

    public void setGame_goal(int game_goal) {
        this.game_goal = game_goal;
    }

    public int getGoals() {
        return goals;
    }

    public void setGoals(int goals) {
        this.goals = goals;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Manager getManager() {
        return manager;
    }

    public void setManager(Manager manager) {
        this.manager = manager;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public Integer getLeague_id() {
        return league_id;
    }

    public void setLeague_id(Integer league_id) {
        this.league_id = league_id;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public void setAll(Club club)
    {
        club.setPoints(0);
        club.setGames(0);
        club.setGoals(0);
    }

    public String clubT() {
        return "Club{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    public String leagueT() {
        //"id | Club | Pts | M | Goals"
        return "  | " + title.substring(0, 3) +
                "  | " + points +
                "      " + games +
                "      " + goals;
    }

    public String table() {
        return "{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", points=" + points +
                ", games=" + games +
                ", goals=" + goals +
                '}';
    }


    @Override
    public String toString() {
        return "Club{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", balance=" + balance +
                ", league_id=" + league_id +
                ", points=" + points +
                ", games=" + games +
                ", goals=" + goals +
                '}';
    }
}
