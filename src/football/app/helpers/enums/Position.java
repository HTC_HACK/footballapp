package football.app.helpers.enums;

public enum Position {

    GOALKEEPER,
    DEFENDER,
    MID_DEFENDER,
    FORWARD,
    MANAGER,

}
