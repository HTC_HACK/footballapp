package football.app.helpers.messages;

public interface MessageHelper {

    String CLUB_CREATED = "Club successfully created";
    String CLUB_NOT_FOUND = "Club not found";
    String CLUB_EXIST = "Club already exist";
    String MANAGER_CREATED = "Manager successfully created";
    String MANAGER_NOT_FOUND = "Manager not found";
    String MANAGER_EXIST = "Manager already exist";
    String PLAYER = "Player successfully created";
    String PLAYER_NOT_FOUND = "Player not found";
    String PLAYER_EXIST = "Player already exist";
    String INSUFFICENT_AMOUNT = "Insufficent amount";
    String FAILED = "Server error";
    String PLAYERS_TRANSFER = "Players in transfer";
    String PLAYER_BUY = "Player successfully bought";
    String PLAYER_TRANSFER = "Player successfully put in transfer";
    String MANAGER_BUY = "Manager successfully bought";
    String CHECK_BALANCE = "Please check your balance";
    String MATCH_STARTED = "Matches started...";
    String MATCH_ENDED = "Matched ended...";
    String CHAMPIONS_ENDED = "Champions is ended";
    String NOT_ENOUGH = "Not enough clubs";
    String WRONG = "Wrong option";
    String UNDER_CONSTRUCTION = "Under construction";
    String NO_MANAGERS = "No managers yet";
    String NO_PLAYERS = "No players yet";
    String NO_CLUBS = "No clubs yet";
    String RESTARTED = "Successfully restarted";
    String BACK_IN = "Returned transfer market";
    String NO_PLAYER_TRANSFER = "No players in transfer market";

}
