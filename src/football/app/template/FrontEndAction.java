package football.app.template;

import football.app.helpers.messages.MessageHelper;
import football.app.league.club.Club;
import football.app.league.playerManager.Manager;
import football.app.league.playerManager.Player;
import football.app.league.service.ClubService;
import football.app.league.service.LeagueService;

import java.util.List;
import java.util.Scanner;

public class FrontEndAction {

    public void leagueChampions() {

        LeagueService leagueService = new LeagueService();
        Scanner scannerInt = new Scanner(System.in);

        boolean active = true;

        while (active) {
            System.out.println("1.League Info. 2.Add Player. 3.Add Clubs. 4.Add Manager");
            System.out.println("5.League table. 6.Transfer list. 7.MatchStart. 8.Select Club");
            System.out.println("9.Club List. 10.Player List. 11.Manager List. 12.MatchResult. 13.Restart Champions 14.Exit");
            System.out.print("Enter option : ");
            int leagueOption = scannerInt.nextInt();
            switch (leagueOption) {
                case 1: {
                    leagueService.leagueInfo();
                    break;
                }
                case 2: {
                    leagueService.createPlayer();
                    break;
                }
                case 3: {
                    leagueService.createClub();
                    break;
                }
                case 4: {
                    leagueService.createManager();
                    break;
                }
                case 5: {
                    leagueService.leagueTable();
                    break;
                }
                case 6: {
                    leagueService.transferList();
                    break;
                }
                case 7: {
                    leagueService.matchStart();
                    break;
                }
                case 8: {
                    Club club = leagueService.chooseClub();
                    if (club != null) {
                        ClubService clubService = new ClubService();
                        boolean clubMenu = true;
                        System.out.println();
                        clubService.clubInfo(club);
                        System.out.println();
                        while (clubMenu) {
                            System.out.println("1.Club info. 2.Balance. 3.Player Stats. 4.InjuryList. 5.Buy Player");
                            System.out.println("6.Buy Manager. 7.Transfer list. 8.Transfer Player. 9.Club Stats. 10.Player list. 11.Manager. 12.Return Player. 13.Logout");
                            int clubOption = scannerInt.nextInt();
                            switch (clubOption) {
                                case 1: {
                                    clubService.clubInfo(club);
                                    break;
                                }
                                case 2: {
                                    clubService.balance(club);
                                    break;
                                }
                                case 3: {
                                    clubService.clubPlayerStats(club);
                                    break;
                                }
                                case 4: {
                                    List<Player> players = leagueService.playersList();
                                    clubService.injuryList(club, players);
                                    break;
                                }
                                case 5: {
                                    Player player = leagueService.choosePlayer(club);
                                    if (player != null) {
                                        Club club1 = leagueService.findClub(player);
                                        clubService.buyPlayer(club, player, club1);
                                    } else {
                                        System.out.println(MessageHelper.PLAYER_NOT_FOUND);
                                    }
                                    break;
                                }
                                case 6: {
                                    Manager manager = leagueService.chooseManager();
                                    if (manager != null) {
                                        clubService.buyManager(club, manager);
                                    } else {
                                        System.out.println(MessageHelper.MANAGER_NOT_FOUND);
                                    }
                                    break;
                                }
                                case 7: {
                                    leagueService.transferList();
                                    break;
                                }
                                case 8: {
                                    List<Player> players = leagueService.playersList();
                                    clubService.transferPlayer(club, players);
                                    break;
                                }
                                case 9: {
                                    clubService.clubStats(club);
                                    break;
                                }
                                case 10: {
                                    leagueService.playersListClub(club);
                                    break;
                                }
                                case 11: {
                                    leagueService.managerClub(club);
                                    break;
                                }
                                case 12: {
                                    List<Player> players = leagueService.playersList();
                                    clubService.returnPlayer(club, players);
                                    break;
                                }
                                case 13: {
                                    clubMenu = false;
                                    break;
                                }
                                default: {
                                    System.out.println(MessageHelper.WRONG);
                                    break;
                                }

                            }
                        }

                    } else {
                        System.out.println(MessageHelper.CLUB_NOT_FOUND);
                    }
                    break;
                }
                case 9: {
                    leagueService.clubList();
                    break;
                }
                case 10: {
                    leagueService.playerList();
                    break;
                }
                case 11: {
                    leagueService.managerList();
                    break;
                }
                case 12: {
                    leagueService.matchesResult();
                    break;
                }
                case 13: {
                    leagueService.restartLeague();
                    break;
                }
                case 14: {
                    active = false;
                    break;
                }
                default: {
                    System.out.println(MessageHelper.WRONG);
                    break;
                }

            }
        }

    }

}
